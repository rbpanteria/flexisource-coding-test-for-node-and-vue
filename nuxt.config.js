require('dotenv').config()

export default {
  ssr: false,
  target: 'static',
  dir: {
    layouts: 'src/app/layouts',
    pages: 'src/app/pages',
    middleware: 'src/app/middleware'
  },
  components: [
    { 
      path: '~/src/app/components', 
      extensions: ['vue'],
      prefix: 'f-' 
    }
  ],
  plugins: [
    '@/src/app/plugins/buefy.js',
    '@/src/app/plugins/http.js',
    '@/src/app/plugins/vee-validate.js'
  ],
  modules: [
    '@nuxt/http'
  ],
  http: {
    baseURL: process.env.API_URL
  },
  buildModules: [
    ['@nuxtjs/dotenv', { only: ['PRODUCTION', 'API_URL'] }],
    '@nuxt/typescript-build',
    '@nuxtjs/composition-api'
  ],
  build: {
    publicPath: process.env.PRODUCTION == 1 ? '../src/app/js' : '/_nuxt/'
  },
  generate: {
    subFolders: false
  },
  serverMiddleware:[
    '~/.koa/dev.js'
  ],
  router: {
    middleware: 'authenticate'
  }
}