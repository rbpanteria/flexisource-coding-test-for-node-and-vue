import Api from './index'

export default {
  path: '/api/',
  handler: Api.callback()
}