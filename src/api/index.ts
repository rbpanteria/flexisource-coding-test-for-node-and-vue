import Koa from 'koa'
import BodyParser from 'koa-bodyparser'
import Routes from './routes'

const Api = new Koa()

Api.use(BodyParser())
Api.use(Routes)

Api.use(async (ctx, next) => {
  return next().catch((error) => {
    if(error.status == 401) {
      ctx.status = 401
      ctx.body = 'You are trying to access a protected resource. Authorization token is required.'
    } else {
      throw error
    }
  })
})

Api.use(async (ctx, next) => {
  ctx.body = 'You are trying to access an unknown resource.'
})

export default Api