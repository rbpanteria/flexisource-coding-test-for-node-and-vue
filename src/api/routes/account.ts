import Router from './router'
import DB from '../knex'
import JWT from 'jsonwebtoken'
import Dotenv from 'dotenv'

const env: any = Dotenv.config().parsed

Router.get('/:id', async (ctx) => {
  const { id } = ctx.params
  const data = await DB('profile').where({ 'id': id })

  ctx.body = data[0]
})

Router.post('/', async (ctx) => {
  const profile = ctx.request.body

  await DB('profile').update({
    'last_name':        profile.last_name,
    'first_name':       profile.first_name,
    'middle_name':      profile.middle_name,
    'delivery_address': profile.delivery_address,
    'billing_address':  profile.billing_address,
  })
  .where({ 'id': profile.id })

  ctx.body = 'Done'
})

Router.post('/authenticate', async (ctx) => {
  let token = null
  const { username, password } = ctx.request.body

  if(!username || !password) {
    ctx.body = 'Username and password are required.'
  } else {
    const profile = await DB('profile').where({ 'username': username, 'password': password })

    if (profile.length) {
      const payload = {
        username: username
      }

      token = JWT.sign(payload, env.JWT_SECRET, {
        expiresIn: '1h'
      })

      ctx.body = { token: token, profile_id: profile[0].id }
    } else {
      ctx.body = null
    }
  }
})

export default Router.routes()