import Router from './router'
import JWT from 'koa-jwt'
import Dotenv from 'dotenv'
import AccountRoutes from './account'

const env: any = Dotenv.config().parsed

Router.use(JWT({ secret: env.JWT_SECRET }).unless({ path: [/^\/account\/authenticate/] }))

Router.use('/account', AccountRoutes)

export default Router.routes()