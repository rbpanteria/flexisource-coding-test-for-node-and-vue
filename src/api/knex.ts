import Dotenv from 'dotenv'

const env: any = Dotenv.config().parsed
const knex = require('knex')({
  client: env.DB_CONNECTION,
  connection: {
    host : env.DB_SERVER,
    user : env.DB_USERNAME,
    password : env.DB_PASSWORD,
    database : env.DB_NAME
  },
  useNullAsDefault: true
})

export default knex