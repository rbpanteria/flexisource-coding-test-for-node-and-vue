import useGlobal from '../composition/useGlobal'
import { ToastProgrammatic as Toast } from 'buefy'

export default function ({ $http }) {
  const { token } = useGlobal()

  $http.onRequest(config => {
    config.headers.set('Authorization', `Bearer ${token.value}`)
  })

  $http.onError(error => {
    if (error.statusCode === 401 || error.statusCode === 500) {
      Toast.open({
        type: 'is-danger',
        message: error
      })
    } 

    return 'Server Error!'
  })
}