import { ref } from '@nuxtjs/composition-api'

const token = ref('')
const profile_id = ref('')

export default function() {
  return {
    profile_id,
    token
  }
}