import useGlobal from '../composition/useGlobal'

export default function ({ route, redirect }) {
  const { token } = useGlobal()

  if(route.path === '/') {
    redirect()
  } else {
    if(!token.value || token.value == '') {
      redirect({ path: '/' })
    } else {
      redirect()
    }
  }
}