exports.up = async function(knex) {
	await knex.schema.hasTable('profile').then(async function(exists) {
		if (!exists) {
			return await knex.schema.createTable('profile', async function(table) {
				table.increments('id').primary().notNullable()
				table.string('last_name', 200).notNullable()
				table.string('first_name', 200).notNullable()
				table.string('middle_name', 200)
				table.string('delivery_address', 200).notNullable()
				table.string('billing_address', 200).notNullable()
				table.string('username', 200).notNullable()
				table.string('password', 200).notNullable()
			})
		}
	})
};

exports.down = function(knex) {
  
};
