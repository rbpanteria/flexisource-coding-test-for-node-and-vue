
exports.seed = function(knex) {
  return knex('profile').del()
    .then(function () {
      return knex('profile').insert([
        {
          last_name:        'Cruz',
          first_name:       'Juan',
          middle_name:      'Dela',
          delivery_address: 'Bulacan',
          billing_address:  'Metro Manila',
          username:         'jdcruz',
          password:         'password',
        },
        {
          last_name:        'User',
          first_name:       'User',
          middle_name:      '',
          delivery_address: 'Makati',
          billing_address:  'Cebu',
          username:         'user',
          password:         'password',
        }
      ]);
    });
};
