import * as path from 'path'

const nodeExternals = require('webpack-node-externals')
const SOURCE_PATH = path.join(__dirname, '..');
console.log(SOURCE_PATH)
module.exports = {
  mode: "production",
  entry: "./src/api/index.ts",
  output: {
    path: path.join(SOURCE_PATH, './'),
    filename: "./index.js"
  },
  resolve: {
    extensions: [".ts", ".tsx", ".js"]
  },
  module: {
    rules: [
      { test: /\.tsx?$/, loader: "ts-loader" }
    ]
  },
  externals: [nodeExternals()]
}