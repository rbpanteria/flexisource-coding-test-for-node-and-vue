# Flexisource Coding Test for Node and Vue

## Installatio
1. Clone https://gitlab.com/rbpanteria/flexisource-coding-test-for-node-and-vue
2. Install `mysql` (https://dev.mysql.com/downloads/)
3. Open commpand prompt and change directory to the root folder then run the command below.

```bash
npm run install
```

# Configuration
1. Modify the `.env` file base on your `mysql` setup. 
3. Open commpand prompt and change directory to the root folder then run the command below. It will install global packages, create the profile table and insert sample data.

```bash
npm run setup
```

## Execution

1. Open commpand prompt and change directory to the root folder then run the following command. You should see `'No issues found'` at the end.

```bash
npm run dev
```

2. Open http://localhost:3000/
3. Username: `jdcruz` / Password: `password` or Username: `user` / Password: `password`
