require('dotenv').config({path: '.env'});

module.exports = {
  development: {
    client: process.env.DB_CONNECTION,
    connection: {
      host : process.env.DB_SERVER,
      user : process.env.DB_USERNAME,
      password : process.env.DB_PASSWORD,
      database : process.env.DB_NAME
    },
    useNullAsDefault: true,
    pool: {
      min: 2,
      max: 10
    },
    migrations: {
      tableName: "knex_migrations"
    }
  }
}